cordova.define("com.perrysystems.appcommunicator.Communicator", function(require, exports, module) { 
/**
 * Constructor
 */
//function Communicator() {}
var Communicator = function() {};

Communicator.prototype.set = function(text,pasteboard) {
  cordova.exec(function(result){
      // result handler
      //alert(result);
    },
    function(error){
      // error handler
      alert("Error" + error);
    }, 
    "Communicator", 
    "set", 
    [text,pasteboard]
  );
}
               
Communicator.prototype.get = function(pasteboard, callBack) {
    cordova.exec(function(result){
        // result handler
                 console.log('Communicator->get()', {result : result, callBack : callBack});
                 if(callBack)
                 callBack.apply(null,[result]);
    },
    function(error){
        // error handler
        alert("Error" + error);
    },
    "Communicator",
    "get",
    [pasteboard]
    );
}

//var communicator = new Communicator();
//e.exports = communicator;
//});

if(!window.plugins) {
    window.plugins = {};
}
if (!window.plugins.communicator) {
    window.plugins.communicator = new Communicator();
}

if (typeof module != 'undefined' && module.exports) {
  module.exports = Communicator;
}
});
