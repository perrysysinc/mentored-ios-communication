#import "Communicator.h"
#import <Cordova/CDV.h>

@implementation Communicator

- (void)set:(CDVInvokedUrlCommand*)command
{
    UIPasteboard* pasteboard = [UIPasteboard pasteboardWithName:command.arguments[1] create:YES];
    
    [pasteboard setString:command.arguments[0]];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:command.arguments[0]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)get:(CDVInvokedUrlCommand*)command
{
    UIPasteboard* pasteboard = [UIPasteboard pasteboardWithName:command.arguments[0] create:NO];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:pasteboard.string];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end