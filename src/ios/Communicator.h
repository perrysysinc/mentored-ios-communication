#import <Cordova/CDV.h>

@interface Communicator : CDVPlugin

- (void)set:(CDVInvokedUrlCommand*)command;
- (void)get:(CDVInvokedUrlCommand*)command;

@end